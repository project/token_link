<?php
/**
 * @file
 * Token link module.
 */

/**
 * Implements hook_menu().
 */
function token_link_menu() {
  $items['admin/config/system/token_link'] = array(
    'title' => 'Token link',
    'description' => 'Configure behavior of tokens on the site.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('token_link_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'token_link.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_form_alter().
 */
function token_link_form_alter(&$form, &$form_state, $form_id) {
  if (variable_get('token_tree_only', 1) !== 1) {
    return;
  }

  module_load_include('inc', 'token', 'token.pages');

  // Use the token_tree_link theme if available (introduced in Token 7.x-1.2)
  if (function_exists('theme_token_tree_link')) {
    array_walk_recursive($form, 'token_link_switch_to_token_link');
  }
}

/**
 * Utility function to change token display.
 */
function token_link_switch_to_token_link(&$item, $key) {
  if (!empty($key) && !empty($item) && $key === '#theme' && $item === 'token_tree') {
    $item = 'token_tree_link';
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function token_link_module_implements_alter(&$implementations, $hook) {
  if (($hook == 'form_FORM_ID_alter' || $hook == 'form_alter') && isset($implementations['token_link'])) {
    // To move an item to the end of the implementation array, we remove it and
    // then add it.
    $group = $implementations['token_link'];
    unset($implementations['token_link']);
    $implementations['token_link'] = $group;
  }
}
