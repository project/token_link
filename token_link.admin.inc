<?php
/**
 * @file
 * Token link settings form.
 */

/**
 * Page callback for token link admin settings form.
 */
function token_link_settings_form($form, &$form_state) {
  module_load_include('inc', 'token', 'token.pages');

  $form['token_tree_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only display token link instead of fully rendered tree'),
    '#description' => t('This will change all fully rendered token trees to a link that secondarily loads tokens.  This will increase performance by speeding up initial page loads.'),
    '#default_value' => variable_get('token_tree_only', TRUE),
  );

  if (!function_exists('theme_token_tree_link')) {
    drupal_set_message('Please upgrade token to 7.x-1.2 or greater.', 'error', FALSE);
  }

  return system_settings_form($form);
}
